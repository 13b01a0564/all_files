#include <stdio.h>
int productofdigits(int);
int sumofdigits(int);

int main() {
	int start = 10;
	int end = 100000;
	for (int i = start; i <= end; i++) {
		int k = (sumofdigits(i));
		if(k == i) {
			printf("%d\n", i);
		}
	}
	return 0;
}
int sumofdigits(int j) {
	int y;
	int p = 0;
	int i = j;
	while (j > 0) {
		y = j % 10;
		j = j / 10;
		 p += y;
	}
	int z = productofdigits(i);
	return (z * p);
}
int productofdigits(int i) {
	int sum = 1;
	int r;
	while ( i > 0) {
		r = i % 10;
		i = i / 10;
		sum *= r;
	}
return sum;
}
