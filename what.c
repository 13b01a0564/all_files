#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
bool IsAllEven(int);

int main(int argc, char* argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage %s number\n", argv[0]);
		return 1;
	}

	int a = atoi(argv[1]);
	if (IsAllEven(a)) {
		printf("%d has", a);
	} else {
		printf("%d does not have", a);
	}
	printf(" the required property\n");
	return 0;
}

bool IsAllEven(int k) {
	while (k > 0) {
		if (k % 2 == 1) {
			return false;
		}
		k /= 10;
	}
	return true;
}
