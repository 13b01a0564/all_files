#include <stdio.h>
int sum_of_divisors(int n);
int isperfect(int j);

int main() {
	int k;
	printf("enter number");
	scanf("%d", &k);
	int sum = sum_of_divisors(k);
	printf("sum_of_divisors = %d\n", sum);
	int total = isperfect(k);
	if(total == k) {
		printf("perfect number");
	}
	return 0;
}
int sum_of_divisors(int k) {
	int sum = 0;
	for (int i =1 ; i <= k; i++) {
		if( k % i != 0 ) {
			continue;
	}
	sum += i;
	}
	return sum;
}
int isperfect(int k) {
	int total = 1;
	int x = 2;
	while (x <= k / 2) {
		if(k % x ==0) {
		total += x;
	}
	x++;
	}
	return total;
}

