#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
bool what(int);

int main(int argc, char* argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage %s number \n", argv[0]);
		return 1;
	}

	int a = atoi(argv[1]);
	if (what(a)) {
		printf("%d has", a);
	} else {
		printf("%d does not have", a);
	}
	printf(" the required property\n");
	return 0;
}

bool what(int k) {
	int asfg = 1;
	while (asfg * asfg < k) {
		asfg++;
		}
	return asfg * asfg == k;
}
