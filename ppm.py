import matplotlib.pyplot as plt
x = int(input("Enter Prey Population"))
y = int(input("enter Predator population"))
z = []
H = []
l = []
Prey_birth_rate = 0.04
Prey_death_rate = 0.0004
Predator_death_rate = 0.25
Predator_birth_rate = 0.001
for t in range (1, 520):
	x += int(Prey_birth_rate * x - Prey_death_rate * x * y)
	y += int(Predator_birth_rate * x * y - Predator_death_rate * y)
	z.append(x)
	H.append(y)
	l.append(t)
plt.xlabel('time')
plt.ylabel('Predator Prey Population')	
plt.plot( l, z, '-', color = 'green')
plt.plot( l, H, '-', color = 'blue')
plt.show()
