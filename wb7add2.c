#include <stdio.h>
#include <stdbool.h>

int multilpesOf10(int);
bool isEven(int);

int main() {
	int n1;
	int n2;
	int n3;
	printf("Enter The Three Numbers: ");
	scanf("%d%d%d", &n1, &n2, &n3);
	int r1 = multiplesOf10(n1);
	int r2 = multiplesOf10(n2);
	int r3 = multiplesOf10(n3);
	int sum = r1 + r2 + r3;
	printf("%d\n", sum);
}

int multiplesOf10(int num) {
	if (isEven(num))
		return (((num / 10) + 1) * 10);
	return ((num / 10) * 10);
}

bool isEven(int num) {
	return (num % 2 == 0);
}


