#include <stdio.h>
#include <limits.h>
int num2digits(int n, int digits[]);
int sum(int k[], int size);
int product(int k[], int size);

int main() {
	int digits[10] = {0};
	for (int n = 10; n < INT_MAX; n++) {
		int nDigits = num2digits(n, digits);
		int p = product(digits, nDigits);
		int s = sum(digits, nDigits);
		if (n == s * p) {
			printf("%10d\n", n);
		}
	}
	return 0;
}

int sum(int d[], int k) {
	int total = 0;
	for (int i = 0; i < k; i++) {
		total += d[i];
	}
	return total;
}

int product(int d[], int k) {
	int prod = 1;
	for (int i = 0; i < k; i++) {
		prod *= d[i];
	}
	return prod;
}

int num2digits(int num, int d[]) {
	int i = 0;
	while (num > 0) {
		d[i] = num % 10;
		i++;
		num /= 10;
	}
	return i;
}
