#include <stdio.h>
#include "util.h"

	int main() {
		int n;
		int a = 2000;
		int b = 8888;
		for (n = a; n <= b; n += 2) {
			if(isAllEven(n)) {
				if(isPerfectSquare(n)) {
				printf("%d\n", n);
			}
		}
	}
		return 0;
}   
