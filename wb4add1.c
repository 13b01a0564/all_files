#include <stdio.h>

int main() {
	int num;
	printf(" Enter The Number: ");
	scanf(" %d ", &num);
	int u_digit = num % 10;
	int h_digit = num / 100;
	int t_digit = (num / 10) % 10;
	int sum = (u_digit * u_digit * u_digit) + (t_digit * t_digit * t_digit) + (h_digit * h_digit * h_digit);
	printf("%d\n", sum == num);
}
