#include "util.h"

bool isAllEven(int k) {
	while (k > 0) {
		if (k % 2 == 1) {
			return false;
		}
		k /= 10;
	}
	return true;
}

int isqrt(int k) {
	int f = 1;
	while (f * f < k) {
		f++;
		}
	return f;
}

bool isPerfectSquare(int k) {
	int i = isqrt(k);
	return i * i == k;
}
